/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pageObjects;

import java.util.List;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author lmangoua
 * 
 * This class stores all the locator and methods
 */
public class HomePage {
    
    WebDriver driver;
    WebElement addUser, fName, lName, uName, pwd, cust, email, cel, save, table, row;
    Select dropdown;
    
    By customer1 = By.xpath("//input[@value='15']"); 
    By customer2 = By.xpath("//input[@value='16']");
         
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }
    
    public void validateMainPage() {
        
        addUser = driver.findElement(By.xpath("//button[@type= 'add']"));
        Assert.assertEquals(true, addUser.isDisplayed());
    }
    
    public void clickAddUserButton() {
         
        try {
            addUser.click();
            Thread.sleep(2000);
        } 
        catch (InterruptedException ex) {
            System.err.println("Failed to click Add User!!");
        }
    }
    
    public void enterFirstName() {
        
        try {
            fName = driver.findElement(By.xpath("//input[@name='FirstName']"));            
            fName.clear();            
            fName.sendKeys("lionel");            
            Thread.sleep(2000);
        } 
        catch (InterruptedException ex) {
            System.err.println("Failed to enter First Name!!");
        }
    }
    
    public void enterLastName() {
        
        try {            
            lName = driver.findElement(By.xpath("//input[@name='LastName']"));            
            lName.clear();            
            lName.sendKeys("Mangoua");            
            Thread.sleep(2000);
        } 
        catch (InterruptedException ex) {
            System.err.println("Failed to enter Last Name!!");
        }
        
    }
    
    public void enterUsername() {
        
        try {
            uName = driver.findElement(By.xpath("//input[@name='UserName']"));            
            uName.clear();            
            uName.sendKeys("Lionel");            
            Thread.sleep(2000);
        } 
        catch (InterruptedException ex) {
            System.err.println("Failed to enter Username!!");
        }
    }
    
    public void enterPassword() {
        
        try {
            pwd = driver.findElement(By.xpath("//input[@name='Password']"));            
            pwd.clear();            
            pwd.sendKeys("Pass1");            
            Thread.sleep(2000);
        } 
        catch (InterruptedException ex) {
            System.err.println("Failed to enter Password!!");
        }
    }
    
    public void clickCustomer() {
        
        try {
            cust = driver.findElement(customer1);            
            cust.click();            
            Thread.sleep(2000);
        }
        catch (InterruptedException ex) {
            System.err.println("Failed to click Customer AAA!!");
        }
    }
    
    public void selectRole() {
        
        try {
            dropdown = new Select(driver.findElement(By.xpath("//select[@name='RoleId']")));
            dropdown.selectByVisibleText("Admin");
            
            Thread.sleep(2000);
        }
        catch (InterruptedException ex) {
            System.err.println("Failed to select Role!!");
        }
    }
    
    public void enterEmail() {
        
        try {
            email = driver.findElement(By.xpath("//input[@name='Email']"));            
            email.clear();            
            email.sendKeys("admin@mail.com");            
            Thread.sleep(2000);
        }
        catch (InterruptedException ex) {
            System.err.println("Failed to enter Email!!");
        }
    }
    
    public void enterCell() {
        
        try {
            cel = driver.findElement(By.xpath("//input[@name='Mobilephone']"));            
            cel.clear();            
            cel.sendKeys("082555");            
            Thread.sleep(2000);
        }
        catch (InterruptedException ex) {
            System.err.println("Failed to enter Cell!!");
        }
    }
    
    public void clickSaveButton() {
        
        try {
            save = driver.findElement(By.xpath("//button[contains(text(),'Save')]"));            
            save.click();            
            Thread.sleep(2000);
        }
        catch (InterruptedException ex) {
            System.err.println("Failed to click Save button!!");
        }
    }
    
    public void verifyUserIsInTheList() {
        
        table = driver.findElement(By.xpath("//table/tbody"));
        
        //Now get all the TR elements from the table 
        List<WebElement> allRows = table.findElements(By.tagName("tr"));

        //And iterate over them, getting the cells 
        for (WebElement row : allRows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));

            //Print the contents of each cell
            for (WebElement cell : cells) {
                if (cell.getText().contentEquals("Lionel")) {
                    System.out.println("");
                    System.out.println("***********************************");
                    System.out.println("*Customer: \"" + cell.getText() + "\" is in the list*");
                    System.out.println("***********************************");
                }
            }
        }
    }
    
    public void closeBrowser() {        
        driver.close();
    }
    
    public void quit() {        
        driver.quit();
    }
        
}
