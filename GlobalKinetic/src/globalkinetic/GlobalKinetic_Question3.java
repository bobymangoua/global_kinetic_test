/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package globalkinetic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.HomePage;

/**
 *
 * @author lmangoua
 * @date: 04/04/2018
 */

public class GlobalKinetic_Question3 {

    /**
     * @param args the command line arguments
     */
    
    private static WebDriver driver = null;
    
    public static void home(String[] args) throws InterruptedException {
        
        //Set Property of ChromeDriver
        System.setProperty("webdriver.chrome.driver", 
                "C:\\Users\\C55-C1881 White\\Documents\\NetBeans_Workplace\\GlobalKinetic_Assessment\\GlobalKinetic\\drivers\\chromedriver.exe");

        //Setup Driver
        driver = new ChromeDriver();
        
        //Open URL
        driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");
        
        //Maximize window
        driver.manage().window().maximize();
        
        HomePage home = new HomePage(driver);
        
        //Validate User List Table
        /*Validate presence of "Add User" button*/
        home.validateMainPage();
        
        //Click "Add User" button 
        home.clickAddUserButton();
        
        /*Enter "First Name" */
        home.enterFirstName();
        
        /*Enter "Last Name" */
        home.enterLastName();
        
        /*Enter "User Name" */
        home.enterUsername();
        
        /*Enter "Password" */
        home.enterPassword();
        
        /*click "Customer" */
        home.clickCustomer();
        
        /*Select "Role" */
        home.selectRole();
        
        /*Enter "Email" */
        home.enterEmail();
        
        /*Enter "Cell" */
        home.enterCell();
        
        /*Click "Save" btn */
        home.clickSaveButton();
        
        /*Verify users created are in the list*/
        home.verifyUserIsInTheList();
        
        //Close browser
        home.closeBrowser();
        
    }
    
}
